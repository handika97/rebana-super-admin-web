import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";
import "./style.css";
import user from "./pages/user";
import home from "./pages/home";
import main from "./pages";
import moment from "moment";
const AuthRoute = ({ component: Component, isLogin, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      isLogin ? (
        <Component {...props} />
      ) : (
        <>
          <Redirect
            to={{
              pathname: "/user/login",
            }}
          />
          <Route path="/user" component={user} />
        </>
      )
    }
  />
);

const App = () => {
  const isLogin = useSelector((state) => state.auth.isLogin);
  return (
    <Router>
      <Switch>
        <AuthRoute path="/app" isLogin={isLogin} component={home} />
        <Route path="/" exact component={main} />
        <Route path="/user" component={user} />
      </Switch>
    </Router>
  );
};

export default App;
