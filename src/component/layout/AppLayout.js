import React, { useState, Fragment, useEffect } from "react";
import { useHistory, useRouteMatch, useParams } from "react-router-dom";
import styled from "styled-components";
import { useSelector, useDispatch } from "react-redux";
import Axios from "axios";
import { BiMenu, BiUserCircle } from "react-icons/bi";
import { FiShoppingCart, FiArrowDown, FiArrowUp } from "react-icons/fi";
import { MdExpandLess, MdExpandMore, MdShoppingBasket } from "react-icons/md";
import { SidebarMenu } from "../../utilities/json";
import { layout } from "../../settings";
import { Logout } from "../../redux/features/authSlice";

const AppLayout = (props) => {
  let history = useHistory();
  let dispatch = useDispatch();
  const [activeSidebar, setActiveSidebar] = useState(false);
  const [windowWidth, setwindowWidth] = useState(window.innerWidth);
  const [windowHeight, setwindowHeight] = useState(window.innerHeight);

  useEffect(() => {
    window.addEventListener("resize", () => {
      setwindowHeight(window.innerHeight);
      setwindowWidth(window.innerWidth);
    });
  }, []);

  return (
    <Fragment>
      <Wrapper
        activeSidebar={activeSidebar}
        windowWidth={windowWidth}
        windowHeight={windowHeight}
      >
        <div className="header">
          <BiMenu
            style={{ cursor: "pointer" }}
            size={24}
            color="black"
            onClick={() => setActiveSidebar(!activeSidebar)}
          />
          <div className="buttonProfile">
            <BiUserCircle size={30} color="black" />
            <div className="logout" onClick={() => dispatch(Logout())}>
              <p>Logout</p>
            </div>
          </div>
        </div>
        <div className="sidebar">
          <div className="sideBar-head">
            {/* <img
              src={require("../../assets/logo.png")}
              className="image"
              alt="logo.png"
            /> */}
            LOGO
          </div>
          <div className="sideBar-Menu">
            {SidebarMenu.map((item) => {
              return (
                <>
                  {activeSidebar && <p className="title-menu">{item.title}</p>}
                  {item.Menu.map((Menus) => {
                    return (
                      <Menu
                        Menu={Menus}
                        activeSidebar={activeSidebar}
                        setActiveSidebar={() => setActiveSidebar(true)}
                      />
                    );
                  })}
                </>
              );
            })}
          </div>
        </div>
        <div className="container-app">{props.children}</div>
      </Wrapper>
    </Fragment>
  );
};

export default AppLayout;

const Wrapper = styled.div`
  .header {
    width: ${(props) =>
      props.activeSidebar
        ? props.windowWidth - layout.sideBar.wide
        : props.windowWidth - layout.sideBar.narrow}px;
    margin-left: ${(props) =>
      props.activeSidebar ? layout.sideBar.wide : layout.sideBar.narrow}px;
    position: fixed;
    height: ${layout.navbarHeight}px;
    background-color: var(--primary-color);
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 0 20px;
    box-sizing: border-box;
    transition: 0.5s;
    .buttonProfile {
      .logout {
        position: absolute;
        background-color: white;
        padding: 10px;
        box-sizing: border-box;
        right: 10px;
        border: 0.5px grey solid;
        border-radius: 5px;
        visibility: hidden;
      }
      :hover .logout {
        visibility: visible;
        cursor: pointer;
      }
      .logout:hover {
        background-color: #64b0f2;
        color: white;
      }
    }
  }
  .sidebar {
    position: fixed;
    width: ${(props) =>
      props.activeSidebar ? layout.sideBar.wide : layout.sideBar.narrow}px;
    height: 100vh;
    background-color: white;
    display: flex;
    flex-direction: column;
    box-sizing: border-box;
    align-items: center;
    transition: 0.5s;
    box-shadow: 0 0 8px rgba(0, 0, 0, 0.03);
    padding-right: 2px;
    padding-bottom: 4px;
    .image {
      width: 70%;
      object-fit: contain;
    }
    .sideBar-head {
      display: flex;
      justify-content: center;
      align-items: center;
      height: ${layout.navbarHeight}px;
    }
    .sideBar-Menu {
      width: 100%;
      overflow: auto;
      height: ${({ windowHeight }) => windowHeight - layout.navbarHeight}px;
      ::-webkit-scrollbar {
        width: 4px;
      }
      ::-webkit-scrollbar-thumb {
        background-color: var(--border-color);
        border-radius: 20px;
      }
      .title-menu {
        padding: 18px 24px;
        color: var(--sub-text-color);
        box-sizing: border-box;
        letter-spacing: 0.5px;
        font-size: 13px;
      }
      .menu {
        display: flex;
        width: 100%;
        padding: ${({ activeSidebar }) =>
          activeSidebar ? "12px 12px 12px 24px" : "12px"};
        display: flex;
        align-items: center;
        justify-content: ${(props) =>
          props.activeSidebar ? "space-between" : "center"};
        cursor: pointer;
        * {
          color: var(--main-text-color);
        }
        p {
          font-weight: 600;
          font-size: 0.95rem;
          color: var(--main-text-color);
          display: ${(props) => (props.activeSidebar ? "flex" : "none")};
        }
        .menu-title {
          flex-direction: row;
          gap: 24px;
          display: flex;
          align-items: center;
          line-height: 1.75;
        }
      }
      .child-menu-hidden {
        visibility: hidden;
        display: flex;
        align-items: center;
        justify-content: flex-start;
        padding-left: 68px;
        margin-top: -20px;
        height: 40px;
        opacity: 0;
        transition: 0.2s;
        p {
          font-size: 14px !important;
          color: var(--sub-text-color);
        }
      }
      .child-menu {
        display: flex;
        width: 100%;
        padding-left: 68px;
        margin-top: 0;
        display: flex;
        align-items: center;
        justify-content: flex-start;
        height: 40px;
        opacity: 1;
        transition: 0.2s;
        p {
          font-size: 14px !important;
          color: var(--sub-text-color);
        }
      }
      .subMenu {
        width: 100%;
        box-sizing: border-box;
        transition: 0.2s;
        overflow: hidden;
        display: flex;
        justify-content: space-between;
        flex-direction: column;
        cursor: pointer;
        p {
          font-weight: 100;
          font-size: 0.95rem;
        }
      }
      .active {
        background-color: #64b0f2;
        * {
          color: white !important;
        }
      }
    }
  }
  .container-app {
    padding: ${layout.navbarHeight}px 0px 0px
      ${(props) =>
        props.activeSidebar ? layout.sideBar.wide : layout.sideBar.narrow}px;
    transition: 0.5s;
    height: 100vh;
    width: 100vw;
    box-sizing: border-box;
    display: flex;
    background-color: #d7d7d7;
  }
`;

const Menu = ({ Menu, activeSidebar, setActiveSidebar = () => {} }) => {
  let history = useHistory();
  const [menuWide, setMenuWide] = useState(false);
  return (
    <Wrapper>
      <div
        className={
          Menu.path === "/" + history.location.pathname.split("/")[2]
            ? "menu active"
            : "menu"
        }
        onClick={() => {
          setMenuWide(!menuWide);
          Menu.subMenu
            ? setActiveSidebar()
            : history.push(
                history.location.pathname.split("/").slice(0, 2).join("/") +
                  Menu.path
              );
        }}
      >
        <div className="menu-title no-user-select">
          {Menu.icon}
          <p classname="no-user-select"> {Menu.Label}</p>
        </div>
        {Menu.subMenu && activeSidebar ? (
          !menuWide ? (
            <div style={{ display: "flex", alignItems: "center" }}>
              <MdExpandMore size={18} onClick={() => setMenuWide(!menuWide)} />
            </div>
          ) : (
            <div style={{ display: "flex", alignItems: "center" }}>
              <MdExpandLess size={18} onClick={() => setMenuWide(!menuWide)} />
            </div>
          )
        ) : null}
      </div>
      {Menu.subMenu && activeSidebar && (
        <div
          className="subMenu"
          style={
            menuWide
              ? {
                  height: Menu.subMenu.length * 40 + "px",
                  visibility: "visible",
                }
              : {
                  height: 0,
                  visibility: "hidden",
                }
          }
        >
          {Menu.subMenu.map((subMenu) => {
            return (
              <div className={menuWide ? "child-menu" : "child-menu-hidden"}>
                <p className="no-user-select"> {subMenu.label}</p>
              </div>
            );
          })}
        </div>
      )}
    </Wrapper>
  );
};
