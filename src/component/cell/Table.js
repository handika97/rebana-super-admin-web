import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { useTable, usePagination, useRowSelect } from "react-table";
import DropDown from "./DropDown";
import Input from "./Input";
import Button from "./Button";
import { staticData } from "../../settings";
import { BsSearch } from "react-icons/bs";
import ReactHTMLTableToExcel from "react-html-table-to-excel";
export default function Table({
  columns,
  data,
  totalPage,
  pageNow,
  excel,
  search,
  detailAction = () => {},
  setSearch = () => {},
  setPageNow = () => {},
  action = () => {},
}) {
  // Use the state and functions returned from useTable to build your UI
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    state: { pageIndex, pageSize },
  } = useTable(
    {
      columns,
      data,
    },
    usePagination
  );
  // Render the UI for your table

  return (
    <Styles>
      <div className="topTable">
        <Input
          Value={search}
          onChange={(e) => setSearch(e)}
          customWidth="200"
          placeholder="search"
          style={{ marginBottom: 0 }}
          id="search"
          icon={<BsSearch />}
        />
      </div>
      {excel && (
        <div>
          <ReactHTMLTableToExcel
            className="btn btn-info"
            table="table"
            filename="ReportExcel"
            sheet="Sheet"
            buttonText="Export excel"
          />
        </div>
      )}
      <TableContainer>
        <table {...getTableProps()} id="table">
          <thead>
            {headerGroups.map((headerGroup) => (
              <tr {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map((column) => (
                  <th {...column.getHeaderProps()}>
                    {column.render("Header")}
                  </th>
                ))}
              </tr>
            ))}
          </thead>
          <tbody {...getTableBodyProps()}>
            {rows.map((row, i) => {
              prepareRow(row);
              return (
                <>
                  <tr {...row.getRowProps()}>
                    {row.cells.map((cell) => {
                      return (
                        <>
                          {cell.column.Header === "Action" ? (
                            <div style={{ width: 150 }}>
                              <DropDownActions
                                onChange={(e) => {
                                  action({
                                    actions: e,
                                    item: cell.row.cells[0].row.original,
                                  });
                                }}
                              />
                            </div>
                          ) : cell.column.Header === "Detail" ? (
                            <Button
                              Title="Detail"
                              onClick={() => {
                                detailAction();
                                action(cell.row.cells[0].row.original);
                              }}
                              className="orange"
                            />
                          ) : cell.column.Header === "Delete" ? (
                            <div style={{ width: 100 }}>
                              <Button
                                Title="Delete"
                                onClick={() => {
                                  detailAction();
                                  action(cell.row.cells[0].row.original);
                                }}
                                className="delete"
                              />
                            </div>
                          ) : (
                            <td {...cell.getCellProps()}>
                              {cell.render("Cell")}
                            </td>
                          )}
                        </>
                      );
                    })}
                  </tr>
                </>
              );
            })}
          </tbody>
        </table>
      </TableContainer>
      {/* <Pagination
        totalPage={totalPage}
        pageNow={pageNow}
        setPageNow={(e) => setPageNow(e)}
      /> */}
    </Styles>
  );
}

const DropDownActions = ({ onChange = () => {} }) => {
  const [value, setValue] = useState("Action");
  return (
    <td style={{ width: 100 }}>
      <DropDown
        noPadding
        Action
        data={staticData.action}
        value={0}
        onChange={(e) => {
          setValue(e);
          onChange(e);
        }}
      />
    </td>
  );
};

const Pagination = ({ totalPage, pageNow, setPageNow = () => {} }) => {
  const [page, setPage] = useState([]);

  useEffect(() => {
    TotalPage();
  }, [pageNow]);

  const TotalPage = () => {
    let json = [];
    for (let i = 0; i < totalPage; i++) {
      json = [...json, { index: i + 1 }];
    }
    setPage(json);
  };
  return (
    <div className="pagination">
      <div
        className="nonActiveButton buttonPagination"
        onClick={() => pageNow > 1 && setPageNow(pageNow - 1)}
      >
        <p>Previous</p>
      </div>
      {page.map((items) => (
        <>
          {items.index === 1 ||
          items.index === pageNow ||
          items.index === page.length ||
          items.index === pageNow - 1 ||
          items.index === pageNow + 1 ||
          (pageNow < 3 && items.index <= 3) ||
          (pageNow === totalPage && items.index >= totalPage - 2) ? (
            items.index === pageNow ? (
              <div className="activeButton buttonPagination">
                <p>{items.index}</p>
              </div>
            ) : (
              <div
                className="nonActiveButton buttonPagination"
                onClick={() => {
                  setPageNow(items.index);
                }}
              >
                <p>{items.index}</p>
              </div>
            )
          ) : items.index === pageNow - 2 ||
            items.index === pageNow + 2 ||
            (pageNow === 1 && items.index === 6) ||
            (pageNow === totalPage && items.index === totalPage - 3) ? (
            <div className="buttonPagination dots">
              <p>...</p>
            </div>
          ) : null}
        </>
      ))}
      <div
        className="nonActiveButton buttonPagination"
        onClick={() => pageNow < totalPage && setPageNow(pageNow + 1)}
      >
        <p>Next</p>
      </div>
    </div>
  );
};

const Styles = styled.div`
  width: 100%;
  .pagination {
    display: flex;
    justify-content: flex-end;
    .buttonPagination {
      border: 1px solid var(--border-input-color);
      border-collapse: collapse;
      border-radius: 2px;
      padding: 10px 12px;
      margin-right: 4px;
      cursor: pointer;
      font-size: 14px;
      color: var(--main-text-color);
      transition: 0.1s;
      p {
        -webkit-user-select: none; /* Chrome 49+ */
        -moz-user-select: none; /* Firefox 43+ */
        -ms-user-select: none; /* No support yet */
        user-select: none;
      }
      &:hover {
        background-color: var(--hover-primary-color);
      }
      &:active {
        background-color: var(--primary-color);
        color: white;
      }
      &.active {
        background-color: var(--primary-color);
        color: white;
        &:hover {
          background-color: var(--primary-color);
        }
      }
      &.dots {
        border: none;
        cursor: default;
        &:hover,
        &:active {
          background-color: unset;
          color: unset;
        }
      }
    }
  }
  .topTable {
    display: flex;
    justify-content: space-between;
    align-items: flex-end;
    padding: 12px 0;
    .showItems {
      display: flex;
      align-items: center;
      gap: 8px;
      p {
        font-size: 14px;
        color: var(--sub-text-color);
      }
    }
  }
`;

const TableContainer = styled.div`
  width: 100%;
  margin-bottom: 24px;
  /* max-height: 445px; */
  overflow-x: auto;
  ::-webkit-scrollbar {
    height: 4px;
  }
  ::-webkit-scrollbar-thumb {
    background-color: var(--border-color);
    border-radius: 20px;
  }
  table {
    border-top: 1px solid var(--border-color);
    width: 100%;
    border-collapse: separate;
    thead {
      font-weight: bold;
      text-align: left;
      th {
        background-color: white;
        position: sticky;
        top: 0;
        padding: 22px;
        border-top: 1px solid white;
      }
    }
    th,
    td {
      padding: 16px 22px;
      border-bottom: 1px solid var(--border-color);
      font-size: 14px;
      color: var(--main-text-color);
      &:first-child {
        width: 5%;
        white-space: nowrap;
      }
    }
    tr:hover {
      background-color: var(--border-color);
    }
  }
`;
