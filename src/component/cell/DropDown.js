import React from "react";
import styled, { css } from "styled-components";

export default function DropDown({
  style,
  full = false,
  value,
  onChange = () => {},
  data,
  label,
  noPadding,
  Action,
  ...rest
}) {
  return (
    <StyledDiv style={style} full={full} noPadding={noPadding}>
      <div className={Action ? "ActionSelectPicker" : "SelectPicker"}>
        <select
          name={label}
          id={label}
          value={value}
          onChange={(e) => onChange(e.target.value)}
          {...rest}
        >
          {label && (
            <option value="0" selected>
              {label}
            </option>
          )}
          {data.map((item) => {
            return (
              <option value={item.id} className="option">
                {item.label}
              </option>
            );
          })}
        </select>
      </div>
    </StyledDiv>
  );
}
const StyledDiv = styled.div`
  .SelectPicker {
    background-color: transparent;
    border: 1px var(--border-input-color) solid;
    border-radius: 2px;
    max-width: 150px;
    box-sizing: content-box;
    ${({ full }) =>
      full &&
      css`
        width: 100%;
        max-width: 100%;
      `}
    select {
      border: none;
      outline: none;
      width: 100%;
      height: 100%;
      padding: ${({ noPadding }) => (noPadding ? "0" : "10px 12px")};
      color: grey;
      border-radius: 2px;
      transition: 0.2s;
    }
    select:focus {
      box-shadow: 0 0 0 1px var(--border-primary-color);
    }
  }
  .ActionSelectPicker {
    background-color: var(--background-dropdown-action);
    border: 1px var(--background-dropdown-action) solid;
    height: 30px;
    max-width: 75px;
    border-radius: 2px;
    select {
      border: none;
      outline: none;
      width: 100%;
      height: 100%;
      padding: ${({ noPadding }) => (noPadding ? "0" : "10px 12px")};
      color: white;
      border-radius: 2px;
      background-color: var(--background-dropdown-action);
      transition: 0.2s;
    }
    .option {
      background-color: white;
      color: black;
    }
  }
`;
