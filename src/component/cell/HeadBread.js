import React from 'react';
import styled from 'styled-components';
import { layout } from '../../settings';

const HeadBread = ({title}) => {
  return (
    <StyledDiv>
      <p>{title}</p>
    </StyledDiv>
  );
}
const StyledDiv = styled.div`
  width: 100%;
  padding: 0 20px;
  box-sizing: border-box;
  display: flex;
  height: ${layout.headBreadHeight}px;
  align-items: center;
  background-color: white;
  margin-bottom: 18px;
  p {
    font-weight: 600;
    font-size: 20px;
    color: var(--main-text-color);
  }
`;

export default HeadBread;