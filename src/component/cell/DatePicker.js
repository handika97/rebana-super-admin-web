import React from "react";
import styled, { css } from "styled-components";

export default function DatePicker({
  style, value, full = false, onChange = () => {},
  ...rest 
}) {
  return (
    <StyledDiv
      style={style}
      full={full}
    >
      <div className="DatePicker">
        <input
          type="date"
          value={value}
          onChange={(e) => onChange(e.target.value)}
          {...rest}
        />
      </div>
    </StyledDiv>
  );
}
const StyledDiv = styled.div`
  .DatePicker {
    background-color: transparent;
    border: 1px var(--border-input-color) solid;
    border-radius: 2px;
    max-width: 150px;
    ${({full}) => full && css`
      width: 100%;
      max-width: 100%;
    `}
    input {
      border: none;
      outline: none;
      width: 100%;
      height: 100%;
      padding: 10px 12px;
      color: grey;
      border-radius: 2px;
      transition: .2s;
    }
    input:focus {
      box-shadow: 0 0 0 1px var(--border-primary-color);
    }
  }
`;
