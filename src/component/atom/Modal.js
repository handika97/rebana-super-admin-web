import React, { useState, useEffect } from "react";
import styled, { css } from "styled-components";
import { FiX } from "react-icons/fi";
import Button from "../cell/Button";
const heightHeader = 50;

export default ({
  children,
  width = "30%",
  headerLabel = "Header Modal",
  cancelBtn = true,
  cancelLabel = "Ngga",
  loading = true,
  okeBtn = true,
  okeLabel = "Iya",
  onClick = () => {},
  footer = true,
  customFooter = "",
  showModal = false,
  toggleModal = () => {},
}) => {
  return (
    <>
      {loading ? (
        <Loading width={width} isModal={showModal}>
          <div class="lds-roller">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
          </div>
        </Loading>
      ) : (
        <Modal width={width} isModal={showModal}>
          <section className="headerModal">
            <div
              className="close"
              onClick={() => {
                toggleModal && toggleModal(false);
              }}
            >
              <FiX />
            </div>
            <Wrapper>
              <center>
                <h5 className="h5">{headerLabel}</h5>
              </center>
            </Wrapper>
          </section>
          <section className="bodyModal">
            <center>
              <Wrapper>{children}</Wrapper>
            </center>
          </section>
          {footer && (
            <div className="footer">
              <Wrapper>
                {!customFooter ? (
                  <center>
                    {okeBtn && (
                      <Button
                        oke
                        onClick={() => {
                          onClick(okeLabel);
                        }}
                        Title={
                          <div
                            style={{
                              justifyContent: "center",
                              alignItems: "center",
                              display: "flex",
                            }}
                          >
                            {/* <MdAdd size={20} /> */}
                            {okeLabel}
                          </div>
                        }
                      />
                    )}
                    {cancelBtn && (
                      <Button
                        small
                        cancel
                        onClick={() => {
                          toggleModal();
                        }}
                        Title={
                          <div
                            style={{
                              justifyContent: "center",
                              alignItems: "center",
                              display: "flex",
                            }}
                          >
                            Batal
                          </div>
                        }
                      />
                    )}
                  </center>
                ) : (
                  customFooter
                )}
              </Wrapper>
            </div>
          )}
        </Modal>
      )}
      <Bg isModal={showModal} />
    </>
  );
};

const Modal = styled.div`
  position: fixed;
  width: ${(props) => props.width};
  left: 50%;
  top: 50%;
  z-index: 4;
  transform: translate(-50%, -50%) scale(1);
  opacity: 1;
  background-color: white;
  border-radius: 12px;
  overflow: hidden;
  visibility: visible;
  transition: 0.2s;
  ${(props) =>
    !props.isModal &&
    css`
      visibility: hidden;
      transform: translate(-50%, -50%) scale(0.6);
      opacity: 0;
    `};
  .headerModal {
    position: relative;
    height: ${heightHeader}px;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  .close {
    position: absolute;
    display: flex;
    align-items: center;
    justify-content: center;
    background-color: white;
    height: ${heightHeader}px;
    width: ${heightHeader}px;
    top: 0;
    right: 0;
    cursor: pointer;
    z-index: 1;
    * {
      font-size: 1.5rem;
    }
  }
  .bodyModal {
    min-height: 36px;
    max-height: 70vh;
    overflow-y: auto;
  }
  .footer {
    /* min-height: ${heightHeader}px; */
    /* padding: 1rem 0; */
  }
  center {
    display: flex;
    flex-direction: row;
    gap: 20px;
    justify-content: center;
    align-items: center;
  }
`;
const Bg = styled.div`
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background-color: rgba(0, 0, 0, 0.7);
  visibility: visible;
  z-index: 3;
  transition: 0.6s;
  ${(props) =>
    !props.isModal &&
    css`
      background-color: rgba(0, 0, 0, 0);
      visibility: hidden;
    `};
`;
const Wrapper = styled.div`
  padding: 0 1rem;
  width: 100%;
  margin: 20px 0px;
`;

const Loading = styled.div`
  position: fixed;
  width: ${(props) => props.width};
  left: 62%;
  top: 50%;
  z-index: 4;
  transform: translate(-50%, -50%) scale(1);
  opacity: 1;
  overflow: hidden;
  visibility: visible;
  transition: 0.2s;
  ${(props) =>
    !props.isModal &&
    css`
      visibility: hidden;
      transform: translate(-50%, -50%) scale(0.6);
      opacity: 0;
    `};
  .lds-roller {
    display: inline-block;
    position: relative;
    width: 80px;
    height: 80px;
  }
  .lds-roller div {
    animation: lds-roller 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
    transform-origin: 40px 40px;
  }
  .lds-roller div:after {
    content: " ";
    display: block;
    position: absolute;
    width: 7px;
    height: 7px;
    border-radius: 50%;
    background: #fff;
    margin: -4px 0 0 -4px;
  }
  .lds-roller div:nth-child(1) {
    animation-delay: -0.036s;
  }
  .lds-roller div:nth-child(1):after {
    top: 63px;
    left: 63px;
  }
  .lds-roller div:nth-child(2) {
    animation-delay: -0.072s;
  }
  .lds-roller div:nth-child(2):after {
    top: 68px;
    left: 56px;
  }
  .lds-roller div:nth-child(3) {
    animation-delay: -0.108s;
  }
  .lds-roller div:nth-child(3):after {
    top: 71px;
    left: 48px;
  }
  .lds-roller div:nth-child(4) {
    animation-delay: -0.144s;
  }
  .lds-roller div:nth-child(4):after {
    top: 72px;
    left: 40px;
  }
  .lds-roller div:nth-child(5) {
    animation-delay: -0.18s;
  }
  .lds-roller div:nth-child(5):after {
    top: 71px;
    left: 32px;
  }
  .lds-roller div:nth-child(6) {
    animation-delay: -0.216s;
  }
  .lds-roller div:nth-child(6):after {
    top: 68px;
    left: 24px;
  }
  .lds-roller div:nth-child(7) {
    animation-delay: -0.252s;
  }
  .lds-roller div:nth-child(7):after {
    top: 63px;
    left: 17px;
  }
  .lds-roller div:nth-child(8) {
    animation-delay: -0.288s;
  }
  .lds-roller div:nth-child(8):after {
    top: 56px;
    left: 12px;
  }
  @keyframes lds-roller {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }
`;
