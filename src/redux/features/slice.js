/* eslint-disable no-unused-vars */
import { createSlice } from "@reduxjs/toolkit";
import Axios from "axios";
import { loginSuccess } from "./authSlice";
import { BaseUrl } from "../../utilities/BaseUrl";
import { logout } from "./authSlice";

export const slice = createSlice({
  name: "main",
  initialState: {},
  reducers: {},
});

export const {} = slice.actions;

export const post = (
  link,
  data,
  ifSuccess = () => {},
  ifError = () => {},
  finallyDo = () => {}
) => async (dispatch, getState) => {
  await Axios.post(BaseUrl + link, data, {
    headers: {
      "Content-Type": "application/json",
      Token: getState().auth.token,
      // "Access-Control-Allow-Origin": "*"
    },
  })
    .then((res) => {
      ifSuccess(res);
    })
    .catch((err) => {
      if (err.response?.status === 401) {
        dispatch(logout());
      } else {
        ifError(err);
      }
    })
    .finally(() => {
      finallyDo();
    });
};

export const patch = (
  link,
  data,
  ifSuccess = () => {},
  ifError = () => {},
  finallyDo = () => {}
) => async (dispatch, getState) => {
  await Axios.patch(BaseUrl + link, data, {
    headers: {
      "Content-Type": "application/json",
      Token: getState().auth.token,
      // "Access-Control-Allow-Origin": "*"
    },
  })
    .then((res) => {
      ifSuccess(res);
    })
    .catch((err) => {
      if (err.response?.status === 401) {
        dispatch(logout());
      } else {
        ifError(err);
      }
    })
    .finally(() => {
      finallyDo();
    });
};

export const get = (
  link,
  ifSuccess = () => {},
  ifError = () => {},
  finallyDo = () => {}
) => async (dispatch, getState) => {
  const { auth } = getState();
  await Axios.get(BaseUrl + link, {
    headers: {
      "Content-Type": "application/json",
      token: auth.token,
    },
  })
    .then((res) => {
      ifSuccess(res);
    })
    .catch((err) => {
      if (err.response?.status === 401) {
        dispatch(logout());
      } else {
        ifError(err);
      }
    })
    .finally(() => {
      finallyDo();
    });
};

export const deleted = (
  link,
  ifSuccess = () => {},
  ifError = () => {},
  finallyDo = () => {}
) => async (dispatch, getState) => {
  const { auth } = getState();
  await Axios.delete(BaseUrl + link, {
    headers: {
      "Content-Type": "application/json",
      token: auth.token,
    },
    data: {},
  })
    .then((res) => {
      ifSuccess(res);
    })
    .catch((err) => {
      if (err.response?.status === 401) {
        dispatch(logout());
      } else {
        ifError(err);
      }
    })
    .finally(() => {
      finallyDo();
    });
};

export default slice.reducer;
