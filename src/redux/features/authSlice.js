import { createSlice } from "@reduxjs/toolkit";
import { post, patch } from "./slice";
import { BaseUrl } from "../../utilities/BaseUrl";
export const auth = createSlice({
  name: "auth",
  initialState: {
    isLogin: false,
    token: "",
    data: "",
  },
  reducers: {
    startAsync: (state) => {
      state.loading = true;
    },
    stopAsync: (state) => {
      state.loading = false;
    },
    loginSuccess: (state, action) => {
      state.isLogin = true;
      state.token = action.payload.token;
      state.data = action.payload;
    },
    logout: (state) => {
      state.isLogin = false;
      state.token = "";
      state.data = "";
    },
  },
});

export const { startAsync, stopAsync, loginSuccess, logout } = auth.actions;

export default auth.reducer;

// ---------------- ACTION ---------------

// import {errorMessage} from '../../utils';
const defaultBody = null;

export const Login = (history, Email, Password, actionsErr = () => {}) => (
  dispatch
) => {
  dispatch(
    post(
      "/kyc/login",
      { email: Email, password: Password },
      (res) => {
        if (res.data.data.role === 3) {
          dispatch(loginSuccess(res.data.data));
          history.replace("/app");
        } else {
          actionsErr();
        }
      },
      (err) => {
        actionsErr();
      }
    )
  );
};
export const Logout = () => (dispatch) => {
  dispatch(logout());
};
