export const layout = {
  sideBar: {
    wide: 250,
    narrow: 70,
  },
  navbarHeight: 70,
  headBreadHeight: 60,
};

export const staticData = {
  status: [
    { id: 1, label: "success" },
    { id: 2, label: "failed" },
    { id: 3, label: "inquiry" },
  ],
  active: [
    { id: 1, label: "active" },
    { id: 2, label: "nonactive" },
  ],
  pagination: [
    { id: 10, label: 10 },
    { id: 25, label: 25 },
    { id: 50, label: 50 },
    { id: 100, label: 100 },
  ],
  action: [
    { id: "Add", label: "Pilih" },
    { id: "Delete", label: "Delete" },
    { id: "Detail", label: "Detail" },
  ],
  // action: [
  //   { id: "edit", label: "Edit" },
  //   { id: "approve", label: "Approve" },
  //   { id: "reject", label: "Reject" },
  // ],
};
