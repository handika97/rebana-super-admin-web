import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import login from "./login";
import AppLayout from "../../component/layout/AppLayout";

const User = ({ match }) => {
  return (
    <Switch>
      <Route path={`${match.url}/login`} component={login} />
      <Redirect exact to={`${match.url}/login`} />
    </Switch>
  );
};

export default User;
