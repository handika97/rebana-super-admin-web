import React from "react";
import {
  Route,
  Switch,
  Redirect,
  useRouteMatch,
  useHistory,
} from "react-router-dom";
import Product from "./Product";
import UserList from "./User";
import OwnerList from "./Owner";
import AdminList from "./Admin";
import CategoryList from "./Category";
import DetailPage from "./Detail";
import AppLayout from "../../component/layout/AppLayout";

const User = ({ match }) => {
  let { url } = useRouteMatch();

  return (
    <AppLayout>
      <Switch>
        <Route path={`${match.url}/product`} component={Product} />
        <Route path={`${match.url}/userlist`} component={UserList} />
        <Route path={`${match.url}/ownerlist`} component={OwnerList} />
        <Route path={`${match.url}/categorylist`} component={CategoryList} />
        <Route path={`${match.url}/adminlist`} component={AdminList} />
        <Route path={`${match.url}/detail/:id`} component={DetailPage} />
        <Redirect exact to={`${match.url}/product`} />
      </Switch>
    </AppLayout>
  );
};

export default User;
