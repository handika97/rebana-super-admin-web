import React, { useState, Fragment, useEffect, useMemo } from "react";
import { useHistory, useRouteMatch, useParams } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { HiUserCircle } from "react-icons/hi";
import styled from "styled-components";
import { post, get } from "../../redux/features/slice";
import HeadBread from "../../component/cell/HeadBread";
import BodyPage from "../../component/cell/BodyPage";
import ContainerBox from "../../component/cell/ContainerBox";
import Container from "../../component/cell/Container";
import Table from "../../component/cell/Table";
import Modal from "../../component/atom/Modal";
import Button from "../../component/cell/Button";
import Message from "../../component/cell/Message";
import { BaseUrl } from "../../utilities/BaseUrl";

const Detail = ({ location }) => {
  const item = location.state;
  const auth = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const history = useHistory();
  let { url } = useRouteMatch();
  const params = useParams();
  const [search, setSearch] = useState("");
  const [modal, setModal] = useState(true);
  const [message, setMessage] = useState(false);
  const [status, setStatus] = useState("");
  const [ProductSelected, setProductSelected] = useState({});
  const [Actions, setActions] = useState("");
  const [Loading, setLoading] = useState(true);
  const [attribute, setAttribute] = useState({
    id: "",
    name: "",
    password: "",
    email: "",
    role_id: "",
  });
  const [Data, setData] = useState([]);

  useEffect(() => {
    // setDate(moment(new Date()).format("YYYY-MM-DD"));
    setLoading(true);
    setModal(true);
    dispatch(
      get(
        `/product/owner/all/${item.id}/${search ? search : "null"}`,
        (res) => {
          console.log(res.data.data);
          setData(res.data.data);
          setStatus("Succeess");
        },
        (err) => {
          setStatus("Error");
        },
        () => {
          setLoading(false);
          setModal(false);
          setMessage(true);
          setTimeout(() => setMessage(false), 3000);
        }
      )
    );
  }, [search]);

  const columns = React.useMemo(
    () => [
      {
        Header: "ID",
        accessor: "id",
      },
      {
        Header: "Name",
        accessor: "name",
      },
      {
        Header: "Description",
        accessor: "description",
      },
      {
        Header: "Price",
        accessor: "price",
      },
      {
        Header: "Status",
        accessor: "status",
      },
      {
        Header: "Detail",
        accessor: "detail",
      },
    ],
    []
  );

  return (
    <Fragment>
      <BodyPage>
        <HeadBread title={`Detail Owner -> ${item.id}`} />
        <Container>
          <ContainerBox
            style={{
              marginBottom: 36,
            }}
          >
            <div style={{ display: "flex", flexDirection: "row" }}>
              {item.photo ? (
                <img
                  src={item.photo.replace("http://localhost:4005", BaseUrl)}
                  alt="Italian Trulli"
                  style={{ height: 200, width: 200 }}
                />
              ) : (
                <HiUserCircle size={200} />
              )}
              <div
                style={{
                  display: "flex",
                  justifyContent: "flex-start",
                  flexDirection: "column",
                  alignItems: "flex-start",
                  padding: 2,
                }}
              >
                <p>Name = {item.name}</p>
                <p>Phone = {item.phone}</p>
                <p>Email = {item.email}</p>
                <p>Address = {item.address}</p>
              </div>
            </div>
            <Table
              columns={columns}
              data={Data}
              search={search}
              detailAction={() => setModal(true)}
              setSearch={(e) => {
                setSearch(e);
              }}
              action={(e) => {
                setProductSelected(e);
              }}
            />
          </ContainerBox>
        </Container>
      </BodyPage>
      <Modal
        showModal={modal}
        toggleModal={() => setModal(false)}
        loading={Loading}
        headerLabel="Detail Product"
        width={300}
        customFooter={
          <Button
            Title="Closed"
            className="orange"
            onClick={() => setModal(false)}
          />
        }
      >
        <div style={{ display: "flex", flexDirection: "row" }}>
          <img
            src={
              ProductSelected.photo
                ? ProductSelected.photo.replace(
                    "http://localhost:4005",
                    BaseUrl
                  )
                : null
            }
            alt="Italian Trulli"
            style={{ height: 200, width: 200 }}
          />
          <div
            style={{
              display: "flex",
              justifyContent: "flex-start",
              flexDirection: "column",
              alignItems: "flex-start",
              padding: 2,
            }}
          >
            <p>Name = {ProductSelected.name}</p>
            <p>Category = {ProductSelected.category_name}</p>
            <p>Price = Rp. {ProductSelected.price}</p>
            <p>Status = {ProductSelected.status}</p>
            <p>Name Seller = {ProductSelected.seller_name}</p>
            <p>Phone Seller = {ProductSelected.seller_phone}</p>
            <p>Address Seller = {ProductSelected.seller_address}</p>
          </div>
        </div>
        <p>Description ={ProductSelected.description}</p>
      </Modal>
      <Message ShowMessage={message} Status={status} />
    </Fragment>
  );
};

export default Detail;

const Filter = styled.div`
  display: flex;
  .w-100 {
    width: 100px;
  }
`;
