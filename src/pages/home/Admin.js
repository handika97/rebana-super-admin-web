import React, { useState, Fragment, useEffect, useMemo } from "react";
import { useHistory, useRouteMatch, useParams } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { HiUserCircle } from "react-icons/hi";
import styled from "styled-components";
import Axios from "axios";
import moment from "moment";
import { post, get } from "../../redux/features/slice";
import HeadBread from "../../component/cell/HeadBread";
import BodyPage from "../../component/cell/BodyPage";
import ContainerBox from "../../component/cell/ContainerBox";
import Container from "../../component/cell/Container";
import Table from "../../component/cell/Table";
import Modal from "../../component/atom/Modal";
import Button from "../../component/cell/Button";
import Input from "../../component/cell/Input";
import Message from "../../component/cell/Message";

const Login = ({ match }) => {
  const auth = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const history = useHistory();
  let { url } = useRouteMatch();
  const params = useParams();
  const [search, setSearch] = useState("");
  const [AdminSelected, setAdminSelected] = useState({});
  const [modal, setModal] = useState(true);
  const [message, setMessage] = useState(false);
  const [status, setStatus] = useState("");
  const [SeePassword, setSeePassword] = useState(false);
  const [Actions, setActions] = useState("");
  const [Loading, setLoading] = useState(true);
  const [attribute, setAttribute] = useState({
    name: "",
    password: "",
    email: "",
    address: "",
    phone: "",
  });
  const [attributeStatus, setAttributeStatus] = useState({
    name: true,
    password: true,
    email: true,
    address: true,
    phone: true,
  });
  const [Data, setData] = useState([]);

  useEffect(() => {
    getData();
  }, [search]);

  const getData = () => {
    dispatch(
      get(
        `/kyc/3/${search ? search : "null"}`,
        (res) => {
          setData(res.data.data);
          setStatus("Succeess");
          setModal(false);
        },
        (err) => {
          setStatus("Error");
          setModal(false);
        },
        () => {
          setLoading(false);
          setMessage(true);
          setTimeout(() => setMessage(false), 3000);
        }
      )
    );
  };

  const columns = React.useMemo(
    () => [
      {
        Header: "ID",
        accessor: "id",
      },
      {
        Header: "Nama",
        accessor: "name",
      },
      {
        Header: "Email",
        accessor: "email",
      },
      {
        Header: "Phone",
        accessor: "phone",
      },
      {
        Header: "Address",
        accessor: "address",
      },
      {
        Header: "Detail",
        accessor: "detail",
      },
    ],
    []
  );

  useEffect(() => {
    if (attribute.email.length > 0) {
      setTimeout(() => {
        validateEmail();
      }, 1000);
    }
  }, [attribute.email]);
  useEffect(() => {
    if (attribute.password.length > 0) {
      setTimeout(() => {
        if (attribute.password.length < 6) {
          setAttributeStatus({ ...attributeStatus, password: false });
        } else {
          setAttributeStatus({ ...attributeStatus, password: true });
        }
      }, 1000);
    }
  }, [attribute.password]);
  useEffect(() => {
    if (attribute.name.length > 0) {
      setTimeout(() => {
        if (attribute.name.length < 2) {
          setAttributeStatus({ ...attributeStatus, name: false });
        } else {
          setAttributeStatus({ ...attributeStatus, name: true });
        }
      }, 1000);
    }
  }, [attribute.name]);
  useEffect(() => {
    if (attribute.phone.length > 0) {
      setTimeout(() => {
        if (attribute.phone.length < 10) {
          setAttributeStatus({ ...attributeStatus, phone: false });
        } else {
          setAttributeStatus({ ...attributeStatus, phone: true });
        }
      }, 1000);
    }
  }, [attribute.phone]);
  useEffect(() => {
    if (attribute.address.length > 0) {
      setTimeout(() => {
        if (attribute.address.length < 2) {
          setAttributeStatus({ ...attributeStatus, address: false });
        } else {
          setAttributeStatus({ ...attributeStatus, address: true });
        }
      }, 1000);
    }
  }, [attribute.address]);

  const createAdmin = () => {
    if (!attribute.name) {
      setAttributeStatus({ ...attributeStatus, name: false });
    } else if (!attribute.phone) {
      setAttributeStatus({ ...attributeStatus, phone: false });
    } else if (!attribute.address) {
      setAttributeStatus({ ...attributeStatus, address: false });
    } else if (!attribute.email) {
      setAttributeStatus({ ...attributeStatus, email: false });
    } else if (!attribute.password) {
      setAttributeStatus({ ...attributeStatus, password: false });
    } else if (
      attributeStatus.email &&
      attributeStatus.password &&
      attributeStatus.phone &&
      attributeStatus.name &&
      attributeStatus.address
    ) {
      setLoading(true);
      dispatch(
        post(
          "/kyc/register",
          {
            name: attribute.name,
            email: attribute.email,
            password: attribute.password,
            phone: attribute.phone,
            address: attribute.address,
            role: 3,
            store_name: "none",
          },
          (res) => {
            setStatus("Success");
            setModal(false);
            getData();
          },
          (err) => {
            setStatus("Error");
          },
          () => {
            setLoading(false);
            setMessage(true);
            setTimeout(() => setMessage(false), 3000);
          }
        )
      );
    }
  };

  function validateEmail() {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return setAttributeStatus({
      ...attributeStatus,
      email: re.test(String(attribute.email).toLowerCase()),
    });
  }

  useEffect(() => {
    if (modal) {
      setAttributeStatus({
        name: true,
        password: true,
        email: true,
        address: true,
        phone: true,
      });
      setAttribute({
        name: "",
        password: "",
        email: "",
        address: "",
        phone: "",
      });
    }
  }, [modal]);

  return (
    <Fragment>
      <BodyPage>
        <HeadBread title="Admin List" />
        <Container>
          <ContainerBox
            style={{
              marginBottom: 36,
            }}
          >
            <Filter>
              <div style={{ width: 100 }}>
                <Button
                  Title="Add"
                  onClick={() => {
                    setModal(true);
                    setActions("Add");
                  }}
                  className="add"
                />
              </div>
            </Filter>
            <Table
              columns={columns}
              data={Data}
              search={search}
              detailAction={() => setModal(true)}
              setSearch={(e) => {
                setSearch(e);
              }}
              action={(e) => {
                setAdminSelected(e);
                setActions("");
              }}
            />
          </ContainerBox>
        </Container>
      </BodyPage>
      <Modal
        showModal={modal}
        toggleModal={() => setModal(false)}
        loading={Loading}
        headerLabel={Actions === "Add" ? "Add Admin" : "Detail User"}
        width={200}
        okeBtn
        onClick={() => createAdmin()}
        customFooter={
          Actions === "Add" ? null : (
            <Button
              Title="Closed"
              className="orange"
              onClick={() => setModal(false)}
            />
          )
        }
      >
        {Actions === "Add" ? (
          <div style={{ width: 400 }}>
            <Input
              full
              className={attributeStatus.name ? "input" : "wrong-input"}
              placeholder="Ketik Nama Anda"
              type={"text"}
              Title="Name"
              Value={attribute.name}
              onChange={(e) => setAttribute({ ...attribute, name: e })}
              Correction={attributeStatus.name}
              correctionText={"Masukan Nama Anda"}
            />
            <Input
              full
              className={attributeStatus.phone ? "input" : "wrong-input"}
              placeholder="Ketik Nomer HP Anda"
              type={"number"}
              Title="Phone"
              Value={attribute.phone}
              onChange={(e) => setAttribute({ ...attribute, phone: e })}
              Correction={attributeStatus.phone}
              correctionText={"Nomer HP Minimal 10 Digit"}
            />
            <Input
              full
              className={attributeStatus.address ? "input" : "wrong-input"}
              placeholder="Ketik Alamat Anda"
              type={"text"}
              Title="Address"
              Value={attribute.address}
              onChange={(e) => setAttribute({ ...attribute, address: e })}
              Correction={attributeStatus.address}
              correctionText={"Masukan Alamat Anda"}
            />
            <Input
              full
              className={attributeStatus.email ? "input" : "wrong-input"}
              placeholder="Ketik Email Anda"
              type={"email"}
              Title="Email"
              Value={attribute.email}
              onChange={(e) => setAttribute({ ...attribute, email: e })}
              Correction={attributeStatus.email}
              correctionText={
                attribute.email ? "Format Email Salah" : "Masukan Email"
              }
            />
            <Input
              full
              password
              className={
                (attributeStatus.password ? "input" : "wrong-input") +
                " password"
              }
              placeholder="Ketik Kata Sandi Anda"
              type={!SeePassword ? "password" : "text"}
              Value={attribute.password}
              Title="Password"
              SeePassword={SeePassword}
              onClick={() => setSeePassword(!SeePassword)}
              Correction={attributeStatus.password}
              correctionText={
                attribute.password.length
                  ? "Password Minimal 6 Digit"
                  : "Masukan Password"
              }
              onChange={(e) => setAttribute({ ...attribute, password: e })}
            />
          </div>
        ) : (
          <div style={{ display: "flex", flexDirection: "row" }}>
            {AdminSelected.photo ? (
              <img
                src={AdminSelected.photo}
                alt="Italian Trulli"
                style={{ height: 200, width: 200 }}
              />
            ) : (
              <HiUserCircle size={200} />
            )}
            <div
              style={{
                display: "flex",
                justifyContent: "flex-start",
                flexDirection: "column",
                alignItems: "flex-start",
                padding: 2,
              }}
            >
              <p>Name = {AdminSelected.name}</p>
              <p>Phone = {AdminSelected.phone}</p>
              <p>Email = {AdminSelected.email}</p>
              <p>Address = {AdminSelected.address}</p>
            </div>
          </div>
        )}
      </Modal>
      <Message ShowMessage={message} Status={status} />
    </Fragment>
  );
};

export default Login;

const Filter = styled.div`
  display: flex;
  .w-100 {
    width: 100px;
  }
`;
