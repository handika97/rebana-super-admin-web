import React, { useState, Fragment, useEffect, useMemo } from "react";
import { useHistory, useRouteMatch, useParams } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { MdAdd } from "react-icons/md";
import styled from "styled-components";
import Axios from "axios";
import moment from "moment";
import { post, get, deleted } from "../../redux/features/slice";
import HeadBread from "../../component/cell/HeadBread";
import BodyPage from "../../component/cell/BodyPage";
import ContainerBox from "../../component/cell/ContainerBox";
import Container from "../../component/cell/Container";
import Table from "../../component/cell/Table";
import Modal from "../../component/atom/Modal";
import Button from "../../component/cell/Button";
import Input from "../../component/cell/Input";
import Message from "../../component/cell/Message";

const Login = ({ match }) => {
  const auth = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const history = useHistory();
  let { url } = useRouteMatch();
  const params = useParams();
  const [search, setSearch] = useState("");
  const [Id, setId] = useState(0);
  const [setOwnerSelected, setsetOwnerSelected] = useState({});
  const [modal, setModal] = useState(true);
  const [message, setMessage] = useState(false);
  const [status, setStatus] = useState("");
  const [SeePassword, setSeePassword] = useState(false);
  const [Actions, setActions] = useState("");
  const [Loading, setLoading] = useState(true);
  const [attribute, setAttribute] = useState({
    id: "",
    name: "",
    password: "",
    email: "",
    role_id: "",
  });
  const [Data, setData] = useState([]);

  useEffect(() => {
    getData();
  }, [search]);

  const getData = () => {
    dispatch(
      get(
        `/kyc/2/${search ? search : "null"}`,
        (res) => {
          setData(res.data.data);
          setStatus("Succeess");
          setModal(false);
        },
        (err) => {
          setStatus("Error");
          setModal(false);
        },
        () => {
          setLoading(false);
          setMessage(true);
          setTimeout(() => setMessage(false), 3000);
        }
      )
    );
  };

  const deleteUser = () => {
    setLoading(true);
    dispatch(
      deleted(
        "/kyc/" + Id,
        (res) => {
          setStatus("Success");
          setModal(false);
          getData();
        },
        (err) => {
          setStatus("Error");
        },
        () => {
          setLoading(false);
          setMessage(true);
          setTimeout(() => setMessage(false), 3000);
        }
      )
    );
  };

  const columns = React.useMemo(
    () => [
      {
        Header: "ID",
        accessor: "id",
      },
      {
        Header: "Nama",
        accessor: "name",
      },
      {
        Header: "Nama Toko",
        accessor: "store_name",
      },
      {
        Header: "Email",
        accessor: "email",
      },
      {
        Header: "Phone",
        accessor: "phone",
      },
      {
        Header: "Address",
        accessor: "address",
      },
      {
        Header: "Action",
        accessor: "action",
      },
    ],
    []
  );

  return (
    <Fragment>
      <BodyPage>
        <HeadBread title="Owner's Store List" />
        <Container>
          <ContainerBox
            style={{
              marginBottom: 36,
            }}
          >
            <Table
              columns={columns}
              data={Data}
              search={search}
              excel={true}
              detailAction={() => setModal(true)}
              setSearch={(e) => {
                setSearch(e);
              }}
              action={(e) => {
                console.log(e.item.id);
                setId(e.item.id);
                e.actions === "Detail"
                  ? history.push({
                      pathname:
                        history.location.pathname
                          .split("/")
                          .slice(0, 2)
                          .join("/") + `/detail/${e.item.id}`,
                      state: e.item,
                    })
                  : setModal(true);
              }}
            />
          </ContainerBox>
        </Container>
      </BodyPage>
      <Message ShowMessage={message} Status={status} />
      <Modal
        showModal={modal}
        toggleModal={() => setModal(false)}
        loading={Loading}
        headerLabel={"Delete User"}
        width={200}
        okeBtn
        onClick={() => {
          deleteUser();
        }}
      >
        <p>Apakah anda yakin untuk menghapus pemilik akun ini?</p>
      </Modal>
    </Fragment>
  );
};

export default Login;

const Filter = styled.div`
  display: flex;
  .w-100 {
    width: 100px;
  }
`;
