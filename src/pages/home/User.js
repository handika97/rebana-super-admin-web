import React, { useState, Fragment, useEffect, useMemo } from "react";
import { useHistory, useRouteMatch, useParams } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { HiUserCircle } from "react-icons/hi";
import styled from "styled-components";
import { post, get } from "../../redux/features/slice";
import HeadBread from "../../component/cell/HeadBread";
import BodyPage from "../../component/cell/BodyPage";
import ContainerBox from "../../component/cell/ContainerBox";
import Container from "../../component/cell/Container";
import Table from "../../component/cell/Table";
import Modal from "../../component/atom/Modal";
import Button from "../../component/cell/Button";
import Message from "../../component/cell/Message";
import { BaseUrl } from "../../utilities/BaseUrl";

const Login = ({ match }) => {
  const auth = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const history = useHistory();
  let { url } = useRouteMatch();
  const params = useParams();
  const [search, setSearch] = useState("");
  const [modal, setModal] = useState(true);
  const [message, setMessage] = useState(false);
  const [status, setStatus] = useState("");
  const [UserSelected, setUserSelected] = useState({});
  const [Actions, setActions] = useState("");
  const [Loading, setLoading] = useState(true);
  const [attribute, setAttribute] = useState({
    id: "",
    name: "",
    password: "",
    email: "",
    role_id: "",
  });
  const [Data, setData] = useState([]);

  useEffect(() => {
    dispatch(
      get(
        `/kyc/1/${search ? search : "null"}`,
        (res) => {
          setData(res.data.data);
          setStatus("Succeess");
          setModal(false);
        },
        (err) => {
          setStatus("Error");
          setModal(false);
        },
        () => {
          setLoading(false);
          setMessage(true);
          setTimeout(() => setMessage(false), 3000);
        }
      )
    );
  }, [search]);

  const columns = React.useMemo(
    () => [
      {
        Header: "ID",
        accessor: "id",
      },
      {
        Header: "Nama",
        accessor: "name",
      },
      {
        Header: "Email",
        accessor: "email",
      },
      {
        Header: "Phone",
        accessor: "phone",
      },
      {
        Header: "Address",
        accessor: "address",
      },
      {
        Header: "Detail",
        accessor: "detail",
      },
    ],
    []
  );

  return (
    <Fragment>
      <BodyPage>
        <HeadBread title="User List" />
        <Container>
          <ContainerBox
            style={{
              marginBottom: 36,
            }}
          >
            <Table
              columns={columns}
              data={Data}
              search={search}
              setSearch={(e) => {
                setSearch(e);
              }}
              detailAction={() => setModal(true)}
              action={(e) => {
                setUserSelected(e);
              }}
            />
          </ContainerBox>
        </Container>
      </BodyPage>
      <Modal
        showModal={modal}
        toggleModal={() => setModal(false)}
        loading={Loading}
        headerLabel="Detail User"
        width={200}
        customFooter={
          <Button
            Title="Closed"
            className="orange"
            onClick={() => setModal(false)}
          />
        }
      >
        <div style={{ display: "flex", flexDirection: "row" }}>
          {UserSelected.photo ? (
            <img
              src={
                UserSelected.photo
                  ? UserSelected.photo.replace("http://localhost:4005", BaseUrl)
                  : null
              }
              alt="Italian Trulli"
              style={{ height: 200, width: 200 }}
            />
          ) : (
            <HiUserCircle size={200} />
          )}
          <div
            style={{
              display: "flex",
              justifyContent: "flex-start",
              flexDirection: "column",
              alignItems: "flex-start",
              padding: 2,
            }}
          >
            <p>Name = {UserSelected.name}</p>
            <p>Phone = {UserSelected.phone}</p>
            <p>Email = {UserSelected.email}</p>
            <p>Address = {UserSelected.address}</p>
          </div>
        </div>
      </Modal>
      <Message ShowMessage={message} Status={status} />
    </Fragment>
  );
};

export default Login;

const Filter = styled.div`
  display: flex;
  .w-100 {
    width: 100px;
  }
`;
