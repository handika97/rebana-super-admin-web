import React, { useState, Fragment, useEffect, useMemo } from "react";
import { useHistory, useRouteMatch, useParams } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { HiUserCircle } from "react-icons/hi";
import styled from "styled-components";
import Axios from "axios";
import moment from "moment";
import { post, get, deleted } from "../../redux/features/slice";
import HeadBread from "../../component/cell/HeadBread";
import BodyPage from "../../component/cell/BodyPage";
import ContainerBox from "../../component/cell/ContainerBox";
import Container from "../../component/cell/Container";
import Table from "../../component/cell/Table";
import Modal from "../../component/atom/Modal";
import Button from "../../component/cell/Button";
import Input from "../../component/cell/Input";
import Message from "../../component/cell/Message";

const Login = ({ match }) => {
  const auth = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const history = useHistory();
  let { url } = useRouteMatch();
  const params = useParams();
  const [search, setSearch] = useState("");
  const [AdminSelected, setAdminSelected] = useState({});
  const [modal, setModal] = useState(true);
  const [Id, setId] = useState(0);
  const [message, setMessage] = useState(false);
  const [status, setStatus] = useState("");
  const [SeePassword, setSeePassword] = useState(false);
  const [Actions, setActions] = useState("");
  const [Loading, setLoading] = useState(true);
  const [attribute, setAttribute] = useState({
    name: "",
  });
  const [attributeStatus, setAttributeStatus] = useState({
    name: true,
  });
  const [Data, setData] = useState([]);

  useEffect(() => {
    getData();
  }, []);

  const getData = () => {
    dispatch(
      get(
        `/category/`,
        (res) => {
          setData(res.data.data);
          setStatus("Succeess");
          setModal(false);
        },
        (err) => {
          setStatus("Error");
          setModal(false);
        },
        () => {
          setLoading(false);
          setMessage(true);
          setTimeout(() => setMessage(false), 3000);
        }
      )
    );
  };

  const columns = React.useMemo(() => [
    {
      Header: "ID",
      accessor: "id",
    },
    {
      Header: "Nama",
      accessor: "name",
    },
    {
      Header: "Delete",
      accessor: "delete",
    },
  ]);

  useEffect(() => {
    if (attribute.name.length > 0) {
      setTimeout(() => {
        if (attribute.name.length < 2) {
          setAttributeStatus({ ...attributeStatus, name: false });
        } else {
          setAttributeStatus({ ...attributeStatus, name: true });
        }
      }, 1000);
    }
  }, [attribute.name]);

  const createAdmin = () => {
    if (!attribute.name) {
      setAttributeStatus({ ...attributeStatus, name: false });
    } else {
      setLoading(true);
      dispatch(
        post(
          "/category/create",
          {
            name: attribute.name,
          },
          (res) => {
            setStatus("Success");
            setModal(false);
            getData();
          },
          (err) => {
            setStatus("Error");
          },
          () => {
            setLoading(false);
            setMessage(true);
            setTimeout(() => setMessage(false), 3000);
          }
        )
      );
    }
  };

  const deleteCategory = () => {
    setLoading(true);
    dispatch(
      deleted(
        "/category/" + Id,
        (res) => {
          setStatus("Success");
          setModal(false);
          getData();
        },
        (err) => {
          setStatus("Error");
        },
        () => {
          setLoading(false);
          setMessage(true);
          setTimeout(() => setMessage(false), 3000);
        }
      )
    );
  };

  useEffect(() => {
    if (modal) {
      setAttributeStatus({
        name: true,
      });
      setAttribute({
        name: "",
      });
    }
  }, [modal]);

  return (
    <Fragment>
      <BodyPage>
        <HeadBread title="Category List" />
        <Container>
          <ContainerBox
            style={{
              marginBottom: 36,
            }}
          >
            <Filter>
              <div style={{ width: 100 }}>
                <Button
                  Title="Add"
                  onClick={() => {
                    setModal(true);
                    setActions("Add");
                  }}
                  className="add"
                />
              </div>
            </Filter>
            <Table
              columns={columns}
              data={
                search
                  ? Data.filter((item) => {
                      return (
                        item.name.toLowerCase().indexOf(search.toLowerCase()) >
                        -1
                      );
                    })
                  : Data
              }
              search={search}
              detailAction={() => setModal(true)}
              setSearch={(e) => {
                setSearch(e);
              }}
              action={(e) => {
                console.log(e);
                setId(e.id);
                setAdminSelected(e);
                setActions("");
              }}
            />
          </ContainerBox>
        </Container>
      </BodyPage>
      <Modal
        showModal={modal}
        toggleModal={() => setModal(false)}
        loading={Loading}
        headerLabel={Actions === "Add" ? "Add Category" : "Delete Category"}
        width={200}
        okeBtn
        onClick={() => (Actions === "Add" ? createAdmin() : deleteCategory())}
      >
        {Actions === "Add" ? (
          <div style={{ width: 400 }}>
            <Input
              full
              className={attributeStatus.name ? "input" : "wrong-input"}
              placeholder="Ketik Nama Category"
              type={"text"}
              Title="Name"
              Value={attribute.name}
              onChange={(e) => setAttribute({ ...attribute, name: e })}
              Correction={attributeStatus.name}
              correctionText={"Masukan Nama Category"}
            />
          </div>
        ) : (
          <p>Apakah Anda Yakin Menghapus Kategory Ini?</p>
        )}
      </Modal>
      <Message ShowMessage={message} Status={status} />
    </Fragment>
  );
};

export default Login;

const Filter = styled.div`
  display: flex;
  .w-100 {
    width: 100px;
  }
`;
