import React, { useState, Fragment, useEffect, useMemo } from "react";
import { useHistory, useRouteMatch, useParams } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { BsEyeSlash, BsEye } from "react-icons/bs";
import styled from "styled-components";
import Axios from "axios";
import moment from "moment";
import { post, get, deleted } from "../../redux/features/slice";
import HeadBread from "../../component/cell/HeadBread";
import BodyPage from "../../component/cell/BodyPage";
import ContainerBox from "../../component/cell/ContainerBox";
import Container from "../../component/cell/Container";
import Table from "../../component/cell/Table";
import DatePicker from "../../component/cell/DatePicker";
import Button from "../../component/cell/Button";
import Modal from "../../component/atom/Modal";
import { staticData } from "../../settings";
import Message from "../../component/cell/Message";
import { BaseUrl } from "../../utilities/BaseUrl";
const Login = ({ match }) => {
  const auth = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const history = useHistory();
  let { url } = useRouteMatch();
  const params = useParams();
  const [search, setSearch] = useState("");
  const [ProductSelected, setProductSelected] = useState({});
  const [modal, setModal] = useState(true);
  const [Loading, setLoading] = useState(true);
  const [Action, setAction] = useState(true);
  const [Data, setData] = useState([]);
  const [message, setMessage] = useState(false);
  const [status, setStatus] = useState("");
  useEffect(() => {
    // setDate(moment(new Date()).format("YYYY-MM-DD"));
    getData();
  }, [search]);

  const getData = () => {
    setLoading(true);
    setModal(true);
    dispatch(
      get(
        `/product/all/${search ? search : "null"}/1`,
        (res) => {
          setData(res.data.data);
          console.log(res.data);
          setStatus("Succeess");
        },
        (err) => {
          console.log(err.response);
          setStatus("Error");
        },
        () => {
          setLoading(false);
          setModal(false);
          setMessage(true);
          setTimeout(() => setMessage(false), 3000);
        }
      )
    );
  };

  const deleteProduct = () => {
    setLoading(true);
    dispatch(
      deleted(
        "/product/" + ProductSelected.id,
        (res) => {
          setStatus("Success");
          setModal(false);
          getData();
        },
        (err) => {
          setStatus("Error");
        },
        () => {
          setLoading(false);
          setMessage(true);
          setTimeout(() => setMessage(false), 3000);
        }
      )
    );
  };

  const columns = React.useMemo(
    () => [
      {
        Header: "ID",
        accessor: "id",
      },
      {
        Header: "Name",
        accessor: "name",
      },
      {
        Header: "Description",
        accessor: "description",
      },
      {
        Header: "Price",
        accessor: "price",
      },
      {
        Header: "Status",
        accessor: "status",
      },
      {
        Header: "Action",
        accessor: "detail",
      },
    ],
    []
  );

  return (
    <Fragment>
      <BodyPage>
        <HeadBread title="Product List" />
        <Container>
          <ContainerBox
            style={{
              marginBottom: 36,
            }}
          >
            <Table
              columns={columns}
              data={Data}
              search={search}
              detailAction={() => setModal(true)}
              setSearch={(e) => {
                setSearch(e);
              }}
              action={(e) => {
                setProductSelected(e);
              }}
              action={(e) => {
                console.log(e);
                setAction(e.actions);
                setModal(true);
                setProductSelected(e.item);
              }}
            />
          </ContainerBox>
        </Container>
      </BodyPage>
      <Modal
        showModal={modal}
        toggleModal={() => setModal(false)}
        loading={Loading}
        headerLabel={Action === "Detail" ? "Detail Product" : "Delete Product"}
        width={300}
        okeBtn
        onClick={() => {
          deleteProduct();
        }}
        customFooter={
          Action === "Detail" ? (
            <Button
              Title="Closed"
              className="orange"
              onClick={() => setModal(false)}
            />
          ) : null
        }
      >
        {Action === "Detail" ? (
          <>
            <div style={{ display: "flex", flexDirection: "row" }}>
              <img
                src={
                  ProductSelected.photo
                    ? ProductSelected.photo.replace(
                        "http://localhost:4005",
                        BaseUrl
                      )
                    : null
                }
                alt="Italian Trulli"
                style={{ height: 200, width: 200 }}
              />
              <div
                style={{
                  display: "flex",
                  justifyContent: "flex-start",
                  flexDirection: "column",
                  alignItems: "flex-start",
                  padding: 2,
                }}
              >
                <p>Name = {ProductSelected.name}</p>
                <p>Category = {ProductSelected.category_name}</p>
                <p>Price = Rp. {ProductSelected.price}</p>
                <p>Status = {ProductSelected.status}</p>
                <p>Name Seller = {ProductSelected.seller_name}</p>
                <p>Phone Seller = {ProductSelected.seller_phone}</p>
                <p>Address Seller = {ProductSelected.seller_address}</p>
              </div>
            </div>
            <p>Description ={ProductSelected.description}</p>
          </>
        ) : (
          <p>Apakah Anda Yakin Akan Menghapus Produk ini?</p>
        )}
      </Modal>
      <Message ShowMessage={message} Status={status} />
    </Fragment>
  );
};

export default Login;

const Filter = styled.div`
  display: flex;
`;
