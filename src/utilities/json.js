import React from "react";
import {
  GrObjectGroup,
  GrNotes,
  GrUserManager,
  GrUserNew,
  GrStakeholder,
} from "react-icons/gr";

const iconSize = 18;

export const SidebarMenu = [
  {
    title: "",
    Menu: [
      {
        icon: <GrObjectGroup size={iconSize} />,
        Label: "Product List",
        path: "/product",
      },
      {
        icon: <GrNotes size={iconSize} />,
        Label: "Category List",
        path: "/categorylist",
      },
      {
        icon: <GrStakeholder size={iconSize} />,
        Label: "Owner's Store List",
        path: "/ownerlist",
      },
      {
        icon: <GrUserManager size={iconSize} />,
        Label: "Admin List",
        path: "/adminlist",
      },
    ],
  },
];
